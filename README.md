# Applaudo studios test management system 

## About

This is a REST API built in java with data persistence to provide the following functionalities:

Create REST endpoints for a basic inventory management system. For this project, the inventory system contains only the single entity named Item. The definitions and detailed requirements are listed below. You will be graded on whether your application performs data retrieval and manipulation based on given use cases exactly as described in the requirements.

It was made using **EJB**, **Wilfly**, **JPA**, **Hibernate** 

Database is in memory **H2**.


## How to run

### Maven

Open a terminal and run the following commands to ensure that you have valid versions of Java (1.8) and Maven (>= 3.3.9 ) installed:

```bash
$ java -version
```

```bash
$ mvn -v
```

#### Using Executable Jar

To create an executable jar run:

```bash
$ mvn clean package
```

To run that application, use the java -jar command, as follows:

```bash
$ java -jar target/applaudo-item-api.jar
```

To exit the application, press **ctrl-c**.

### H2 Database configuration

This application is running an H2 database instance inside the application server.

# REST API

The REST API to the item app is described below.

## Create a new Item

### Request

`POST /applaudo-item-api/api/item`

    curl -i -H 'Accept: application/json' -H 'Content-Type: application/json' -X POST -d "{\"itemId\":1,\"itemName\":\"item_x\",\"itemBuyingPrice\":50.0,\"itemSellingPrice\":55.0,\"itemStatus\":\"AVAILABLE\"}" http://localhost:8080/applaudo-item-api/api/item

### Response

    HTTP/1.1 201 Created
    Date: Mon, 31 Jan 2022 01:12:27 GMT
    Status: 201 Created
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 104

    {"itemBuyingPrice":50.0,"itemId":1,"itemName":"item_x","itemSellingPrice":55.0,"itemStatus":"AVAILABLE"}

## Create a new Item with id already used

### Request

`POST /applaudo-item-api/api/item`

    curl -i -H 'Accept: application/json' -H 'Content-Type: application/json' -X POST -d "{\"itemId\":1,\"itemName\":\"item_x\",\"itemBuyingPrice\":50.0,\"itemSellingPrice\":55.0,\"itemStatus\":\"AVAILABLE\"}" http://localhost:8080/applaudo-item-api/api/item

### Response

    HTTP/1.1 400 Bad Request
    Date: Mon, 31 Jan 2022 01:12:27 GMT
    Status: 400 Bad Request
    Connection: keep-alive
    Content-Length: 0

    {}

## Get list of items based on name and status

### Request

`GET /api/item/?itemStatus=AVAILABLE&itemName=item_x`

    curl -i -H 'Accept: application/json' -H 'Content-Type: application/json' http://localhost:8080/applaudo-item-api/api/item?itemStatus=AVAILABLE&itemName=item_x

### Response

    HTTP/1.1 200 OK
    Date: Mon, 31 Jan 2022 01:12:27 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: 2

    [ {"itemBuyingPrice":50.0,"itemId":1,"itemName":"item_x","itemSellingPrice":55.0,"itemStatus":"AVAILABLE"}
    ]

## Get a specific Item by id

### Request

`GET /applaudo-item-api/api/item/{itemid}`

    curl -i -H 'Accept: application/json' -H 'Content-Type: application/json' http://localhost:8080/applaudo-item-api/api/item/1

### Response

    HTTP/1.1 200 OK
    Date: Mon, 31 Jan 2022 01:12:27 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 104

    {"itemBuyingPrice":50.0,"itemId":1,"itemName":"item_x","itemSellingPrice":55.0,"itemStatus":"AVAILABLE"}

## Get a non-existent Item

### Request

`GET /applaudo-item-api/api/item/{itemid}`

    curl -i -H 'Accept: application/json' -H 'Content-Type: application/json' http://localhost:8080/applaudo-item-api/api/item/9999

### Response

    HTTP/1.1 404 Not Found
    Date: Mon, 31 Jan 2022 01:12:27 GMT
    Status: 404 Not Found
    Connection: keep-alive
    Content-Type: application/json
    Content-Length: 35

    {}

