package com.esa.applaudo.item.api.service;

import java.util.List;

import javax.ejb.Remote;

import com.esa.applaudo.item.api.domain.Item;

@Remote
public interface ItemService {

	public Item createItem(Item item);
	public Item findItem(int id);
	public List<Item> findByStatusAndName(String itemStatus, String itemName);
	
}
