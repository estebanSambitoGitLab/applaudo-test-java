package com.esa.applaudo.item.api.dto.util;

import com.esa.applaudo.item.api.domain.Item;
import com.esa.applaudo.item.api.dto.ItemDTO;

public class ItemConverter {

	public static ItemDTO convertToDTO(Item item) {
		ItemDTO wTem = new ItemDTO();
		wTem.setItemId(item.getItemId());
		wTem.setItemName(item.getItemName());
		wTem.setItemStatus(item.getItemStatus());
		wTem.setItemBuyingPrice(item.getItemBuyingPrice());
		wTem.setItemSellingPrice(item.getItemSellingPrice());
		return wTem;
	}
	
	public static Item convertToEntity(ItemDTO item) {
		Item wTem = new Item();
		wTem.setItemId(item.getItemId());
		wTem.setItemName(item.getItemName());
		wTem.setItemStatus(item.getItemStatus());
		wTem.setItemBuyingPrice(item.getItemBuyingPrice());
		wTem.setItemSellingPrice(item.getItemSellingPrice());
		return wTem;
	}
}
