package com.esa.applaudo.item.api.audit;

import java.io.Serializable;
import java.sql.Timestamp;

public interface AuditableEntity extends Serializable {

	/*@Column
	private String itemEnteredByUser;
	
	@Column
	private String itemLastModifiedByUser;
	*/
	
	Timestamp getItemEnteredByDate();

	void setItemEnteredByDate(Timestamp dateCreated);

	Timestamp getItemLastModifiedDate();

	void setItemLastModifiedDate(Timestamp lastUpdated);

}
