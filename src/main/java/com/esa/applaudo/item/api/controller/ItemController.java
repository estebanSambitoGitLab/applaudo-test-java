package com.esa.applaudo.item.api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import com.esa.applaudo.item.api.domain.Item;
import com.esa.applaudo.item.api.dto.ItemDTO;
import com.esa.applaudo.item.api.dto.util.ItemConverter;
import com.esa.applaudo.item.api.service.ItemService;

@Path("/api/item")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ItemController {

	@Context
	private SecurityContext context;

	@EJB
	private ItemService service;

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ "role-client" })
	public Response getItem(@PathParam("id") int id) {
		Item findItem = service.findItem(id);
		if(findItem == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		return Response.ok(ItemConverter.convertToDTO(findItem), MediaType.APPLICATION_JSON).build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ "role-client" })
	public Response getItems(
			@QueryParam("itemStatus") String itemStatus,
			@QueryParam("itemName") String itemName) {
		List<Item> listItems = service.findByStatusAndName(itemStatus,itemName);
		List<ItemDTO> listItemsResponse = new ArrayList<ItemDTO>(); 
		listItems.stream().forEach(x -> listItemsResponse.add(ItemConverter.convertToDTO(x)));
		return Response.ok(listItemsResponse, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@RolesAllowed({ "role-admin" })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)	
	public Response addItem(@Valid ItemDTO item) {
		
		Item findItem = service.findItem(item.getItemId());
		if(findItem != null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Item createItem = service.createItem(ItemConverter.convertToEntity(item));
		return Response.status(Status.CREATED).type(MediaType.APPLICATION_JSON).
				entity(ItemConverter.convertToDTO(createItem)).build();
	}

}
