package com.esa.applaudo.item.api.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Item DTO class to be received in the API
 * @author esalazar
 *
 */
public class ItemDTO {
	@NotNull
	private int itemId;
	
	@NotNull
	@Size(max = 25)	
	private String itemName;

	@NotNull
	private Double itemBuyingPrice;
	
	@NotNull	
	private Double itemSellingPrice;
	
	@NotNull	
	private String itemStatus;

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getItemBuyingPrice() {
		return itemBuyingPrice;
	}

	public void setItemBuyingPrice(Double itemBuyingPrice) {
		this.itemBuyingPrice = itemBuyingPrice;
	}

	public Double getItemSellingPrice() {
		return itemSellingPrice;
	}

	public void setItemSellingPrice(Double itemSellingPrice) {
		this.itemSellingPrice = itemSellingPrice;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	

	
}
