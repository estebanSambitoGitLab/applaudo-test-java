package com.esa.applaudo.item.api.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.esa.applaudo.item.api.audit.AuditableEntity;
import com.esa.applaudo.item.api.audit.AuditableListener;

@Entity
@Table(name="item")
@EntityListeners(AuditableListener.class)
@NamedQueries(
	@NamedQuery(name = "findByStatusAndNameLower", query = "select e from Item e where lower(e.itemName) = :itemName and e.itemStatus = :itemStatus ")
)
public class Item implements AuditableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private int itemId;
	
	@Column
	private String itemName;
	
	@Column
	private Double itemBuyingPrice;
	
	@Column
	private Double itemSellingPrice;
	
	@Column
	private String itemStatus;

	private String itemEnteredByUser;
	private Timestamp itemEnteredByDate;
	private Timestamp itemLastModifiedDate;
	private String itemLastModifiedByUser;
	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemEnteredByUser() {
		return itemEnteredByUser;
	}

	public void setItemEnteredByUser(String itemEnteredByUser) {
		this.itemEnteredByUser = itemEnteredByUser;
	}

	public Timestamp getItemEnteredByDate() {
		return itemEnteredByDate;
	}

	public void setItemEnteredByDate(Timestamp itemEnteredByDate) {
		this.itemEnteredByDate = itemEnteredByDate;
	}

	public Double getItemBuyingPrice() {
		return itemBuyingPrice;
	}

	public void setItemBuyingPrice(Double itemBuyingPrice) {
		this.itemBuyingPrice = itemBuyingPrice;
	}

	public Double getItemSellingPrice() {
		return itemSellingPrice;
	}

	public void setItemSellingPrice(Double itemSellingPrice) {
		this.itemSellingPrice = itemSellingPrice;
	}

	public Timestamp getItemLastModifiedDate() {
		return itemLastModifiedDate;
	}

	public void setItemLastModifiedDate(Timestamp itemLastModifiedDate) {
		this.itemLastModifiedDate = itemLastModifiedDate;
	}

	public String getItemLastModifiedByUser() {
		return itemLastModifiedByUser;
	}

	public void setItemLastModifiedByUser(String itemLastModifiedByUser) {
		this.itemLastModifiedByUser = itemLastModifiedByUser;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemBuyingPrice=" + itemBuyingPrice
				+ ", itemSellingPrice=" + itemSellingPrice + ", itemStatus=" + itemStatus + ", itemEnteredByUser="
				+ itemEnteredByUser + ", itemEnteredByDate=" + itemEnteredByDate + ", itemLastModifiedDate="
				+ itemLastModifiedDate + ", itemLastModifiedByUser=" + itemLastModifiedByUser + "]";
	}

	
	
	
	

}
