package com.esa.applaudo.item.api.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.esa.applaudo.item.api.domain.Item;

@Stateless
public class ItemServiceImpl implements ItemService {
	
	@PersistenceContext
	private  EntityManager em;
	
	public Item createItem(Item item) {
			em.persist(item);
		return item;
	}

	public Item findItem(int id) {
		Item wItem = em.find(Item.class, id);
		return wItem;
	}

	@Override
	public List<Item> findByStatusAndName(String itemStatus, String itemName) {
		TypedQuery<Item> createNamedQuery = em.createNamedQuery("findByStatusAndNameLower", Item.class);
		createNamedQuery.setParameter("itemName", itemName);
		createNamedQuery.setParameter("itemStatus", itemStatus);
		List<Item> resultList = createNamedQuery.getResultList();
		return resultList;
	}

}
