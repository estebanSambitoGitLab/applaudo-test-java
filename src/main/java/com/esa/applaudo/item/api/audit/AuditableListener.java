package com.esa.applaudo.item.api.audit;

import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class AuditableListener {
	@PrePersist
	void preCreate(AuditableEntity auditable) {
		Timestamp now = Timestamp.from(Instant.now());
		auditable.setItemEnteredByDate(now);
		auditable.setItemLastModifiedDate(now);
	}

	@PreUpdate
	void preUpdate(AuditableEntity auditable) {
		Timestamp now = Timestamp.from(Instant.now());
		auditable.setItemLastModifiedDate(now);
	}
}
